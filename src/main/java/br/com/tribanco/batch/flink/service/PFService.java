package br.com.tribanco.batch.flink.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PFService implements FlinkService {

    private static final Logger log = LoggerFactory.getLogger(PFService.class);

    public Boolean shouldRun() {
        return true;
    }

    @Override
    public String name() {
        return "PF";
    }

}
