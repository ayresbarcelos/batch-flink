package br.com.tribanco.batch.flink.service;

import com.jayway.jsonpath.JsonPath;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static org.springframework.util.StringUtils.isEmpty;

@Component
public class CRMService implements FlinkService {

    private static final Logger log = LoggerFactory.getLogger(CRMService.class);

    private static final OkHttpClient client = new OkHttpClient();
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final DateTimeFormatter serviceDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final DateTimeFormatter paramDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final String JOB_DATE_START_KEY = "job.date.start";
    public static final String JOB_DATE_END_KEY = "job.date.end";

    @Value("${job.crm.endpoint:}")
    private String endpoint;

    @Value("${job.crm.client.id:}")
    private String clientId;

    @Value("${job.crm.authorization:}")
    private String auth;

    @Value("${job.process.type:}")
    private String type;

    @Override
    public Boolean shouldRun() {
        checkArgument(!isEmpty(endpoint), "Parameter job.crm.endpoint required");
        checkArgument(!isEmpty(clientId), "Parameter job.crm.client.id required");
        checkArgument(!isEmpty(auth), "Parameter job.crm.auth required");
        checkArgument(!isEmpty(type), "Parameter job.process.type required");
        checkArgument(type.equalsIgnoreCase("DAILY") ||
            type.equalsIgnoreCase("MONTHLY"), "Parameter job.process.type required");
        String token = getToken(endpoint, clientId, auth);
        return checkExecutionPermission(endpoint, token, clientId, type);
    }

    @Override
    public String name() {
        return "CRM";
    }

    /**
     *
     * Caso job.process.type=DAILY então --job.date.start=<primeiro dia do mês atual> --job.date.end=<data do dia atual>
     * Exemplo DAYLY: --job.date.start=2017-10-01 --job.date.end=2017-10-18
     *
     * Caso job.process.type=MONTHLY então --job.date.start=<primeiro dia do segundo mês anterior ao mês atual> --job.date.end=<primeiro dia do mês atual>
     * Exemplo MONTHLY: --job.date.start=2017-08-01 --job.date.end=2017-10-01
     *
     * @param params
     */
    @Override
    public void processParameters(Map<String, String> params) {
        LocalDate startDate;
        LocalDate endDate;
        if ("DAILY".equalsIgnoreCase(type)) {
            startDate = LocalDate.now().withDayOfMonth(1);
            endDate = LocalDate.now();
        } else {
            startDate = LocalDate.now().withDayOfMonth(1).minusMonths(2);
            endDate = LocalDate.now().withDayOfMonth(1);
        }
        if (isEmpty(params.get(JOB_DATE_START_KEY))) {
            params.put(JOB_DATE_START_KEY, startDate.format(paramDateFormatter));
        }
        if (isEmpty(params.get(JOB_DATE_END_KEY))) {
            params.put(JOB_DATE_END_KEY, endDate.format(paramDateFormatter));
        }
    }

    public Boolean checkExecutionPermission(String url, String accessToken, String clientId, String processingType) {
        String date = LocalDate.now().format(serviceDateFormatter);
        String requestType = getProcessingType(processingType);
        String requestUrl = String.format("%s/crm/cliente/indicadores/executacarga/%s/%s", url, requestType, date);
        Request request = new Request.Builder()
                .url(requestUrl)
                .addHeader("client_id", clientId)
                .addHeader("access_token", accessToken)
                .get()
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                log.error("Execution denied for type {}, date {}. Status code: {}. Response body: {}", type, date, response.code(), response.body().string());
                return false;
            }
            String responseBody = response.body().string();
            String jsonExpMensagem = "$.mensagem";
            String mensagem = JsonPath.parse(responseBody).read(jsonExpMensagem, String.class);
            String jsonExpStatus = "$.status";
            Integer status = JsonPath.parse(responseBody).read(jsonExpStatus, Integer.class);
            log.info("Check permission result: {}, {}", mensagem, status);
            return response.isSuccessful();
        } catch (IOException e) {
            throw new RuntimeException("Token error", e);
        }
    }

    private String getProcessingType(String processingType) {
        if (processingType.equalsIgnoreCase("daily")) {
            return "diario";
        } else if (processingType.equalsIgnoreCase("monthly")) {
            return "mensal";
        } else {
            throw new RuntimeException("Invalid processing type: " + processingType);
        }
    }

    public String getToken(String url, String clientId, String authorization) {
        RequestBody formBody = FormBody.create(JSON,
                "{ \"grant_type\" : \"client_credentials\" }");
        Request request = new Request.Builder()
                .url(String.format("%s/oauth/access-token", url)) //https://apihlg-tribanco.sensedia.com/production/v1/oauth/access-token")
                .addHeader("content-type", "application/json")
                .addHeader("client_id", clientId)
                .addHeader("authorization", String.format("Basic %s", authorization))
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new RuntimeException("Token error: " + response.body().string());
            }
            String jsonExp = "$.access_token";
            String responseBody = response.body().string();
            String token = JsonPath.parse(responseBody).read(jsonExp, String.class);
            log.info("Token: {}", token);
            return token;
        } catch (IOException e) {
            throw new RuntimeException("Token error", e);
        }
    }

}
