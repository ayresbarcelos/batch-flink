package br.com.tribanco.batch.flink.service;

import java.util.Map;

public interface FlinkService {

    public Boolean shouldRun();

    public String name();

    default void processParameters(Map<String, String> params){}
}
