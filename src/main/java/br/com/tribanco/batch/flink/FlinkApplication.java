package br.com.tribanco.batch.flink;

import br.com.tribanco.batch.flink.service.FlinkService;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.common.SSHException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import net.schmizz.sshj.userauth.UserAuthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.util.StringUtils.isEmpty;

@SpringBootApplication
public class FlinkApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(FlinkApplication.class);
    private static final String FLINK_SSH_HOST_KEY = "flink.ssh.host";
    private static final String FLINK_SSH_USER_KEY = "flink.ssh.user";
    private static final String FLINK_SSH_PW_KEY = "flink.ssh.password";
    private static final String JOB_JAR_KEY = "job.jar";
    private static final String JOB_CLASS_KEY = "job.class";
    private static final String JOB_NAME_KEY = "job.name";
    private static final String FLINK_EXEC_PATH_KEY = "flink.exec.path";

    private final Environment env;

    @Autowired
    public FlinkApplication(Environment env) {
        this.env = env;
    }

    @Autowired
    public List<FlinkService> services;

    public static void main(String[] args) throws Exception {
        SpringApplication springApplication =
        new SpringApplicationBuilder()
                .sources(FlinkApplication.class)
                .web(false)
                .build();
		springApplication.run(args);
	}

	@Override
	public void run(String... strings) throws Exception {
        Integer exitCode = 0;

        validateParameters();

        String hostname = env.getProperty(FLINK_SSH_HOST_KEY);
        String username = env.getProperty(FLINK_SSH_USER_KEY);
        String password = env.getProperty(FLINK_SSH_PW_KEY);
        String jobName = env.getProperty(JOB_NAME_KEY);

        checkShouldRun(jobName);

        try (SSHClient ssh = new SSHClient()) {
            ssh.addHostKeyVerifier(new PromiscuousVerifier());
            ssh.connect(hostname);
            try {
                ssh.authPassword(username, password);
                try (Session session = ssh.startSession()) {
                    exitCode = executeCommand(session);
                }
            } finally {
                ssh.disconnect();
            }
        } catch (UnknownHostException t) {
            log.error("Could not connect to server {}", hostname, t);
        } catch (UserAuthException t) {
            log.error("Could not connect. Verify user/password", t);
        } catch (SSHException t) {
            log.error("Could not connect", t);
        } catch (Exception t) {
            log.error("Error", t);
        }
        System.exit(exitCode);
	}

    private void validateParameters() {
        validateKey(FLINK_SSH_HOST_KEY);
        validateKey(FLINK_SSH_USER_KEY);
        validateKey(FLINK_SSH_PW_KEY);
        validateKey(JOB_JAR_KEY);
        validateKey(JOB_CLASS_KEY);
        validateKey(JOB_NAME_KEY);
    }

    private void validateKey(String key) {
        checkArgument(!isEmpty(key), String.format("param %s required", key));
    }

    private Integer executeCommand(Session session) throws IOException {
        String flinkCommandLine = buildCommandLine(env);
        log.info(flinkCommandLine);
        final Session.Command cmd = session.exec(flinkCommandLine);
        logInputStream(cmd.getInputStream());

        String errorLog = IOUtils.readFully(cmd.getErrorStream()).toString();
        if (!isEmpty(errorLog)) {
            log.error(errorLog);
        }
        String exitErrorMessage = cmd.getExitErrorMessage();
        if (!isEmpty(exitErrorMessage)) {
            log.error(exitErrorMessage);
        }
        cmd.join(5, TimeUnit.SECONDS);
        Integer exitCode;
        if (shouldReschedule(errorLog)) {
            exitCode = 2;
        } else {
            exitCode = cmd.getExitStatus();
        }
        return exitCode;
    }

    private boolean shouldReschedule(String errorLog) {
        if (!isEmpty(errorLog) && errorLog.contains("NoResourceAvailableException")) {
            return true;
        } else {
            return false;
        }
    }

    private void checkShouldRun(String jobName) {
        FlinkService service = findService(jobName);
        if (!service.shouldRun()) {
            log.info("Should run condition false, exiting...");
            System.exit(0);
        }
    }

    private void processParameters(Map<String, String> params) {
        String jobName = env.getProperty(JOB_NAME_KEY);
        FlinkService service = findService(jobName);
        service.processParameters(params);

    }

    private FlinkService findService(String jobName) {
        for (FlinkService service : services) {
            if (service.name().equalsIgnoreCase(jobName)) {
                return service;
            }
        }
        throw new RuntimeException("Invalid job.name: " + jobName);
    }

    private String buildCommandLine(Environment env) {
        String flinkHome = env.getProperty(FLINK_EXEC_PATH_KEY);
        String flinkJar = env.getProperty(JOB_JAR_KEY);
        String flinkClass = env.getProperty(JOB_CLASS_KEY);
        String format = "%s run -c %s %s %s";
        Map<String, String> map = listCommandLineProps(env);
        processParameters(map);
        String flinkJarParams = "";
        for (String key : map.keySet()) {
            if (!key.startsWith("flink") && !key.equals(JOB_JAR_KEY) && !key.equals(JOB_CLASS_KEY)) {
                String value = map.get(key);
                if (!isEmpty(value)) {
                    flinkJarParams += String.format(" --%s %s", key, value);
                } else {
                    log.error("Param {} has empty value", key);
                }
            }
        }
        return String.format(format, flinkHome, flinkClass, flinkJar, flinkJarParams);
    }

    private Map<String, String> listCommandLineProps(Environment env) {
        Map<String, String> map = new HashMap<>();
        for(Iterator it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext(); ) {
            PropertySource propertySource = (PropertySource) it.next();
            if (propertySource instanceof SimpleCommandLinePropertySource) {
                String[] propertyNames = ((SimpleCommandLinePropertySource) propertySource).getPropertyNames();
                for (String propertyName : propertyNames) {
                    //https://jira.spring.io/browse/SPR-10241
                    map.put(propertyName, (String) propertySource.getProperty(propertyName));
                }
            }
        }
        return map;
    }

    private static void logInputStream(InputStream is) {
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                log.info(line);
            }

        } catch (IOException e) {
            log.error("Error reading inputStream", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

}
