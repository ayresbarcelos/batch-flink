
Exemplos de execução
```
job.crm.endpoint

job.name=[PF, CRM]

java -jar flink-0.0.1-SNAPSHOT.jar 
--job.jar=/home/abarcelos/dev/apps/flink-1.3.2/examples/batch/WordCount.jar 
--flink.exec.path=/home/abarcelos/dev/apps/flink-1.3.2/bin/flink 
--job.name=CRM 
--job.class=org.apache.flink.examples.java.wordcount.WordCount 
--job.crm.endpoint=https://apihlg-tribanco.sensedia.com/production/v1 
--job.crm.client.id=f46f97be-dd0c-3fd8-9bf1-b1da935a36a5 
--job.crm.authorization=ZjQ2Zjk3YmUtZGQwYy0zZmQ4LTliZjEtYjFkYTkzNWEzNmE1OjJkNzc2N2ZhLWUwOTgtMzY2Yy1hNjM0LWI5M2MzMmExMTc2Mw== 
--job.crm.type=daily

```